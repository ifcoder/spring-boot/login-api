/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ifcoder.login.loginapi.api.controller;

import com.ifcoder.login.loginapi.domain.service.CatalogoUserService;
import com.ifcoder.login.loginapi.domain.model.User;
import com.ifcoder.login.loginapi.domain.repository.UserRepository;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jose
 */
@AllArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {
    
    private UserRepository userRepository; 
    private CatalogoUserService catalogoUserService;

    @GetMapping("/")
    public List<User> list() {
        return userRepository.findAll();        
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> buscar(@PathVariable Long id) {
        return userRepository.findById(id)
        .map(professor -> ResponseEntity.ok(professor))
        .orElse(ResponseEntity.notFound().build());
        
        // outra forma de fazer:
        // Optional<Professor> profFound = professorRepository.findById(id);        

        // if (profFound.isPresent())
        //     return ResponseEntity.ok(profFound.get());
        // else
        //     return ResponseEntity.notFound().build();
        
    }

    
    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public User add(@Valid  @RequestBody User user) {
        //UUID uuid = UUID.randomUUID();
        //user.setId(uuid.toString());
        return catalogoUserService.salvar(user);
    }
}
