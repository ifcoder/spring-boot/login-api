
package com.ifcoder.login.loginapi.domain.service;

import com.ifcoder.login.loginapi.domain.model.User;
import com.ifcoder.login.loginapi.domain.repository.UserRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jose
 */
@AllArgsConstructor
@Service
public class LoginUserService {
    private UserRepository userRepository;
    
    @Transactional
    public boolean cofereLogin(User user){
        Optional<User> userDB = userRepository.findByEmail(user.getEmail());
       
        if(userDB.get().getPassword().equals(user.getPassword()))
            return true;
        else
            return false;
                
    }
    
    
}
