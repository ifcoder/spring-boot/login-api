
package com.ifcoder.login.loginapi.domain.service;

import com.ifcoder.login.loginapi.domain.exception.NegocioException;
import com.ifcoder.login.loginapi.domain.model.User;
import com.ifcoder.login.loginapi.domain.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jose
 */
@AllArgsConstructor
@Service
public class CatalogoUserService {
    
    private UserRepository userRepository;
    /**
     * @param user
     * @return
     */
    @Transactional
    public User salvar(User user){
        boolean emailEmUso = userRepository.findByEmail(user.getEmail())
        .stream()
        .anyMatch(userEncontrado -> !userEncontrado.equals(user));     
        
        if(emailEmUso)
            throw new NegocioException("Já existe um usuario com este email");
        
        return userRepository.save(user);
    
    }
    
    @Transactional
    public void excluir(Long userId){
        userRepository.deleteById(userId);
    }
}
