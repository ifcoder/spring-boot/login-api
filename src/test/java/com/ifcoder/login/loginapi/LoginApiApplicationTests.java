package com.ifcoder.login.loginapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginApiApplicationTests {

    public static void main(String[] args) {
        SpringApplication.run(LoginApiApplicationTests.class, args);
    }

}
